<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $fillable = [
        'id',
        'title',
        'description',
        'img',
        'updated_at',
        'created_at',
        'user_id',
     ];
    public function user() {
        return $this->belongsTo(User::class);
    }
}
