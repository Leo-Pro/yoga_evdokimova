<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
        'id',
        'datetime',
        'title',
        'address',
        'description',
        'updated_at',
        'created_at',
        'user_id',
     ];
    public function user() {
        return $this->belongsTo(User::class);
    }
}
