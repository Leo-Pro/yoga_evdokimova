<?php

namespace App\Tools;

use App\Models\Schedule;

class WeekGenerator
{

    protected $getWeekdayDB;
    protected $weekShift = 0;


    public function __construct($weekShift)
    {
        $this->weekShift = $weekShift;
        $this->getWeekdayDB = Schedule::whereBetween('datetime', [$this->getWeekday()[0], $this->getWeekday()[count($this->getWeekday()) - 1]])->get();
    }

    public function getWeekday()
    {
        $dayShift = 7 * $this->weekShift;
        $weekday = array();
        for($i = $dayShift; $i < 7 + $dayShift;  $i++){
            $day = \carbon\carbon::parse()->locale('ru')->timezone('Europe/Moscow')->weekday($i)->format('Y-m-d');
            array_push($weekday, $day);
        } return $weekday;
    }

    protected function getTime()
    {
        $time = array();
        foreach($this->getWeekdayDB as $item){
            array_push($time, \carbon\carbon::parse($item->datetime)->format('H:i'));
            }
            $listTime = array_unique($time);
            sort($listTime);
            return $listTime;
    }

        protected function scheduleTableTemplate()
        {
            $field = array();
            for($i = 0; $i < count($this->getTime()); $i++){
                for($j = 0; $j <= count($this->getWeekday()); $j++){
                    $field[$i][$j] = '';
                }
            }
            return $field;
        }
        public function scheduleTableData()
        {
            $dataField = $this->scheduleTableTemplate();
            for($i = 0; $i < count($this->getTime()); $i++){
                $dataField[$i][0] = $this->getTime()[$i];
            };
            foreach($this->getWeekdayDB as $data){
                $getTime = \carbon\carbon::parse($data->datetime)->format('H:i');
                $getDate = \carbon\carbon::parse($data->datetime)->format('Y-m-d');
                $keyTime = array_search($getTime,  $this->getTime());
                $keyDate = array_search($getDate, $this->getWeekday());
                $dataField[$keyTime][$keyDate + 1] = $data;
            };
            return $dataField;
        }
}
