<?php

namespace App\Http\Controllers;

use App\Tools\WeekGenerator;
use App\Models\Program;
use App\Models\Feedback;

use App\Http\Requests\FeedbackRequest;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $generator = new WeekGenerator(0);
        return view('pages.index', [
            'listProgram'=> Program::paginate(3),
            'weekday' => $generator->getWeekday(),
            'listSchedule' => $generator->scheduleTableData(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\FeedbackRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store_feedback(FeedbackRequest $request)
    {
        $feedback = new Feedback();
        $feedback->name = $request->input('name');
        $feedback->description = $request->input('description');

        $feedback->save();
        return redirect()->route('index');
    }

        /**
     * Display the specified resource.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program)
    {
        return view('pages.program', [
            'item' => $program,
        ]);
    }
}
