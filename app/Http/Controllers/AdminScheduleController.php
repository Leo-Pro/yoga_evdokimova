<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use App\Models\User;
use App\Http\Requests\ScheduleRequest;

class AdminScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.schedule.index', [
            'listSchedules'=>Schedule::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schedule.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScheduleRequest $request)
    {
        $schedule = new Schedule();
        $schedule->user_id = $request->user()->id;
        $schedule->datetime = $request->input('datetime');
        $schedule->title = $request->input('title');
        $schedule->address = $request->input('address');
        $schedule->description = $request->input('description');
        $schedule->save();
        return redirect()->route('admin.schedule.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        return view('admin.schedule.edit', [
            'item' => $schedule,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(ScheduleRequest $request, User $user, Schedule $schedule)
    {
        $this->authorize('update', $schedule, $user);
        $schedule->user_id = $request->user()->id;
        $schedule->date = $request->input('date');
        $schedule->time = $request->input('time');
        $schedule->title = $request->input('title');
        $schedule->address = $request->input('address');
        $schedule->description = $request->input('description');
        $schedule->save();
        return redirect()->route('admin.schedule.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Schedule $schedule)
    {
        $this->authorize('delete', $schedule, $user);
        $schedule->delete();
        return redirect()->route('admin.schedule.index');
    }
}
