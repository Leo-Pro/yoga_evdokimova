<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Certificate;
use App\Http\Requests\ProgramRequest;

class AdminCertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.certificate.index',[
            'listCertificate'=>Certificate::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.certificate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProgramRequest $request)
    {
        $certificate = new Certificate();
        $certificate->user_id = $request->user()->id;
        $certificate->title = $request->input('title');
        $certificate->description = $request->input('description');
        $certificate->img = $request->file('img')->store('certificate', 'public');
        $certificate->save();
        return redirect()->route('admin.certificate.show', $certificate->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function show(Certificate $certificate)
    {
        return view('admin.certificate.store', [
            'item' => $certificate,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function edit(Certificate $certificate)
    {
        return view('admin.certificate.edit', [
            'item' => $certificate,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function update(ProgramRequest $request, User $user, Certificate $certificate)
    {
        $this->authorize('update', $certificate, $user);
        $certificate->img = $request->file('img')->store('certificate', 'public');
        $certificate->title = $request->input('title');
        $certificate->description = $request->input('description');

        $certificate->save();

        return redirect()->route('admin.post.show', $certificate->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Certificate $certificate)
    {
       $this->authorize('delete', $certificate, $user);
        $certificate->delete();
        return redirect()->route('admin.certificate.index');
    }
}
