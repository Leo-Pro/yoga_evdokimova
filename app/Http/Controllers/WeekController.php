<?php

namespace App\Http\Controllers;

use App\Tools\WeekGenerator;

class WeekController extends Controller
{
    public function index($week_shift)
    {
        if(is_numeric($week_shift)){
            $generator = new WeekGenerator($week_shift);
            return view('output.schedule', [
                'weekday' => $generator->getWeekday(),
                'listSchedule' => $generator->scheduleTableData(),
            ]);
        }
    }
}
