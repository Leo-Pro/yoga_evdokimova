<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Program;
use App\Models\User;

use App\Http\Requests\ProgramRequest;

use Illuminate\Support\Str;

class AdminProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.program.index', [
            'listProgram'=>Program::paginate(9),
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.program.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ProgramRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProgramRequest $request)
    {
        $program = new Program();
        $program->user_id = $request->user()->id;
        $program->title = $request->input('title');
        $program->description = $request->input('description');
        $program->short_description = Str::limit($request->input('description'), 197);
        $program->img = $request->file('img')->store('program', 'public');
        $program->save();
        return redirect()->route('admin.program.show', $program->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program)
    {
        return view('admin.program.store', [
            'item' => $program,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program)
    {
        return view('admin.program.edit', [
            'item' => $program,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProgramRequest  $request
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(ProgramRequest $request, User $user, Program $program)
    {
        $this->authorize('update', $program, $user);
        $program->user_id = $request->user()->id;
        $program->title = $request->input('title');
        $program->description = $request->input('description');
        $program->short_description = Str::limit($request->input('description'), 197);
        $program->img = $request->file('img')->store('program', 'public');

        $program->save();

        return redirect()->route('admin.program.show', $program->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Program $program)
    {
        $this->authorize('delete', $program, $user);
        $program->delete();
        return redirect()->route('admin.program.index');
    }
}
