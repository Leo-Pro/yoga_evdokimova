<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProgramRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'title'=> 'required',
        'description'=> ['string', 'required'],
        'short_description'=> 'max:200',
        'img'=> 'required',
        ];
    }
    public function messages(){
        return [
            'title.required'  => 'Нету заголовка мероприятия',
            'description.required' => 'Описание мероприятия обязательно',
            'img.required'=>'нету изображения'
        ];
    }
}
