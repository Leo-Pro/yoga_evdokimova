
let weekShift = 0;

document.getElementById("previousWeekButton").onclick = () => {
    weekShift --;
    indexWeekShift(weekShift);
};
document.getElementById("resetWeekButton").onclick = () => {
    weekShift = 0;
    indexWeekShift(weekShift);
};
document.getElementById("nextWeekButton").onclick = () => {
    weekShift ++;
    indexWeekShift(weekShift);
};


indexWeekShift = (shiftIndex) => {
    fetch(/schedule/ + shiftIndex)
    .then((response) => {
        return response.text();
    }).then((data) => {
        document.getElementById('ScheduleList').innerHTML = data;
    });
}
