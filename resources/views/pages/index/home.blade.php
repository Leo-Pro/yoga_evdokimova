<div class="hero_area">
    <section class=" slider_section position-relative">
        <div class="container">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="detail-box">
                                    <h1>
                                        FLEUR de LIS <br />
                                        <span>

                      </span>
                                    </h1>
                                    <h2>Клуб йоги творчества и саморазвития</h2>
                                    <hr>
                                    <h3>О Себе</h3>
                                    <p>Первый раз йогой я начала заниматься самостоятельно в 7 лет по библиотечным книгам. Но через некоторое время забыла
                                        об этом полностью. Йога вернулась ко мне только через 18 лет. До этого был разного рода фитнес, танцы, спортивные занятия. Но осознанно я начала заниматься 4 года назад.</p>
                                    <p>Йога – сама по себе является для меня вдохновением.
                                        Вам нужно только один раз влюбиться в практику,
                                        ощутить ее красоту и гармонию.</p>
                                    <p>В современном обществе существует огромное количество разных стилей и видов йоги.
                                        Но для меня йога - едина. Это целостная система психофизического развития человека.</p>
                                    <div class="btn-box">
                                        <a href="{{ route('index')  }}" class="btn-1">
                                            Записаться
                                        </a>
                                        <a href="{{ route('index')  }}#contact" class="btn-2">
                                            Контакты
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

</div>
