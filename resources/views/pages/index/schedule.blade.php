<section class="client_section layout_padding" id="schedule">
    <a href="#schedule"></a>
    <div class="container layout_padding2-top">
        <div class="heading_container">
            <h2>
                Расписание
            </h2>
        </div>
    </div>
    <div class="container">
        <table class="table table-hover" id="ScheduleList">
            <thead>
                <tr>
                    <th scope="col">время</th>
                    @foreach ($weekday as $day)
                    <th scope="col">{{ \carbon\carbon::parse($day)->format('d.m') }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($listSchedule as $data)
                <tr>
                    @foreach ($data as $item)
                    @if(gettype($item) == 'string' && $item !== '')
                    <th scope="row">{{ $item }}</th>
                    @elseif(is_object($item))
                    <td>
                        {{ $item->title }}<br>
                        {{ $item->address }}<br>
                        {{ $item->description }}
                    </td>
                    @elseif($item === '')
                    <td></td>
                    @endif
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="float-right text-right">
            <input type="submit" id="previousWeekButton" value="предыдущая" class="btn btn-warning">
            <input type="submit" id="resetWeekButton" value="текущая" class="btn btn-light">
            <input type="submit" id="nextWeekButton" value="следующая" class="btn btn-success">
        </div>
    </div>
</section>
