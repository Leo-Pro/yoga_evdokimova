@extends('layouts.yoga')

@section('content')
    @include('pages.index.home')
    @include('pages.index.schedule')
    @include('pages.index.program')
    @include('pages.index.certificates')
    @include('pages.index.contact')
    @include('pages.index.feedback')
@endsection
