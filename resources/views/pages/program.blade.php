@extends('layouts.yoga')

@section('content')
    <section class="blog-details-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-details">
                        <h2>{{ $item->title }}</h2>
                        <div class="blog-preview">
                            <img class="col-md-6" src="{{ asset('/storage/' . $item->img) }}" alt="">
                        </div>

                        @foreach(explode(PHP_EOL, $item->description) as $paragraph)
                        <p>{{{ $paragraph }}}</p>
                        @endforeach
                        <div class="blog-gallery">
                            <div class="row">
                                <div class="col-md-4">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="blog-meta"><p><i class="material-icons">автор: </i> {{ $item->user->name }} </p></div>

                                @if ($item->created_at < $item->updated_at)
                                <span class="ed-cata"><i class="material-icons"> изменён: </i> {{ $item->updated_at->format('d.m.y') }}</span>
                                @else
                                <span class="ed-cata"><i class="material-icons">дата публикации: </i> {{ $item->created_at->format('d.m.y') }}</span>
                                @endif
                            </div>
                            <div class="col-sm-5 text-left text-sm-right pt-4 pt-sm-0">
                                <div class="ed-social">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
