<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Basic -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Site Metas -->
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Brighton</title>

    <!-- slider stylesheet -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css" />

    <!-- bootstrap core css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css    ') }}" />
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script>
        function openNav() {
            document.getElementById("myNav").style.width = "100%";
        }

        function closeNav() {
            document.getElementById("myNav").style.width = "0%";
        }
    </script>
    <!-- fonts style -->
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700|Poppins:400,700|Roboto:400,700&display=swap" rel="stylesheet" />
</head>
<body>
    <header class="header_section">
        <div class="container">
            <nav class="navbar navbar-expand-lg custom_nav-container ">
                <a class="navbar-brand" href="{{ route('index') }}">
                    <img style="width: 60px"src="images/logo.png" alt="" />
                    <span>
              FLEUR de LIS
            </span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <div class="d-flex ml-auto flex-column flex-lg-row align-items-center">
                        <ul class="navbar-nav  ">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('index') }}">О себе</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('index') }}/#schedule">Расписание</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('index') }}/#program">Мероприятия</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('index') }}/#certificates">Сертефикаты</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('index') }}/#contact">Отзывы</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
        @yield('content')
    <footer class="container-fluid footer_section row">
        <p class="mx-auto"> 
        <img style="width: 70px" src="images/logo.png" alt="" /> FLEUR de LIS - клуб йоги творчества и саморазвития
        </p>
    </footer>
</body>
</html>
