@extends('layouts.app')

@section('content')
<div class="container">
    <a class="btn btn-primary"type="submit" href="{{ route('admin.schedule.create') }}">Добавить</a>
    <br>
    <table class="table table-striped">
        <caption style="caption-side:top">Расписание занятий</caption>
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Дата</th>
                <th scope="col">Время</th>
                <th scope="col">Название</th>
                <th scope="col">Адрес</th>
                <th scope="col">Прочее</th>
                <th scope="col">Редактировать</th>
                <th scope="col">Удалить</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($listSchedules as $item)
            <tr>
                <th scope="row">{{ $item->id }}</th>
                <td>{{ Carbon\Carbon::parse($item->datetime)->format('d.m.Y') }}</td>
                <td>{{ Carbon\Carbon::parse($item->datetime)->format('H:i') }}</td>
                <td>{{ $item->title }}</td>
                <td>{{ $item->address }}</td>
                <td>{{ $item->description }}</td>
                <td>
                    <a href="{{ route('admin.schedule.edit', $item->id) }}" class="btn btn-primary">редактировать</a>
                </td>
                <td>
                    <form action="{{ route('admin.schedule.destroy', $item->id) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <input class="btn btn-primary" type="submit" value="удалить">
                    </form>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>
</div>
@endsection
