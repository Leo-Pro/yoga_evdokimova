@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Расписание</h1>
    <form  class="m-5" method="post" action="{{ route('admin.schedule.update', $item->id) }}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="time">Дата и время</label>
            <input type="datetime-local" class="form-control" id="datetime" name="datetime" value="{{ $item->datetime }}">
        </div>
        <div class="form-group">
            <label for="title">Название</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ $item->title }}">
        </div>
        <div class="form-group">
            <label for="address">Адрес</label>
            <input type="text" class="form-control" id="address" name="address" value="{{ $item->address }}">
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea">Прочее</label>
            <textarea class="form-control" id="exampleFormControlTextarea" rows="10" name="description">{{ $item->description }}</textarea>
        </div>
        <div class="btn-group">
            <input class="btn btn-primary"type="submit" value="Сохранить">
            <input class="btn btn-link"type="reset" value="Сбросить">
        </div>
    </form>
</div>
@endsection
