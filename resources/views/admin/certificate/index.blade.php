@extends('layouts.app')

@section('content')
<section class="offer_section hero_next_section-margin layout_padding" id="certificates">
    <div class="container">
        <div class="heading_container">
            <h2>
                сертефикаты
            </h2>
        </div>
        <div class="row">
            @foreach ($listCertificate as $item)
            <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                <img class="col-form-label" style="width: 250px" src="{{ asset('/storage/' . $item->img)}}" alt="{{ $item->title }}">
                <a href="{{ route('admin.certificate.show', $item->id) }}">детали...</a>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
