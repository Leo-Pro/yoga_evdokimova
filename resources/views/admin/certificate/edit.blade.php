@extends('layouts.app')

@section('content')
<div class="container">
    <form class="m-5" enctype="multipart/form-data" action="{{ route('admin.program.update', $item->id) }}" method="post">
    @csrf
    @method('PUT')
      <div class="form-group">
            <label for="exampleFormControlInput">Название</label>
            <input type="text" class="form-control" id="exampleFormControlInput" name="title" placeholder="title" value="{{ $item->title }}">
        </div>
        <div class="from-group">
            <img src="{{ asset('/storage/' . $item->img) }}" alt="">
        </div>
        <div class="form-group">
            <label for="exampleFormControlFileImadge">image</label>
            <input type="file" class="form-control-file" id="exampleFormControlFileImadge" name="img" value="{{ $item->img }}">
        </div>
        <div class="form-group">
           <label for="exampleFormControlTextarea">Описание</label>
           <textarea class="form-control" id="exampleFormControlTextarea" rows="10" name="description">{{ $item->description }}</textarea>
      </div>
       <div class="btn-group">
          <input class="btn btn-primary"type="submit" value="Сохранить">
          <input class="btn btn-link"type="reset" value="Сбросить">
      </div>
    </form>
</div>

@endsection
