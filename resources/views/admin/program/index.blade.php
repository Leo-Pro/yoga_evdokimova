@extends('layouts.app')

@section('content')
<section class="client_section layout_padding" id="program">
    <a href="#program"></a>
    <div class="container layout_padding2-top">
        <div class="heading_container">
            <h2>
                Мероприятия
            </h2>
        </div>
    </div>
    <div class="events-page-section spad">
        <div class="container">
            <div class="row">
               @foreach ($listProgram as $item)
                <div class="col-lg-4 col-md-6">
                    <div class="blog-item">
                        <img src="{{ asset('/storage/' . $item->img) }}" alt="">
                        <div class="bi-text">
                            <h2><a href="{{ route('admin.program.show', $item->id) }}">{{ $item->title }}</a></h2>

                        </div>
                        <div class="bi-footer">
                            <div class="bi-cata"><p>{{ $item->short_description }}</p></div>
                            <div class="bi-social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                               <a href="#"><i class="fa fa-twitter"></i></a>

                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="site-pagination pt-3">
              {{ $listProgram->links() }}
            </div>
        </div>
    </div>
</section>
@endsection
