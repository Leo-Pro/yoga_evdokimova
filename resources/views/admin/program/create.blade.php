@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Мероприятие</h1>
    <form  class="m-5" method="post" enctype="multipart/form-data" action="{{ route('admin.program.store') }}">
        @csrf
        <div class="form-group">
            <label for="title">Название</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="title">
        </div>
        <div class="form-group">
            <label for="exampleFormControlImadge">image</label>
            <input type="file" class="form-control-file" id="exampleFormControlImadge" name="img">
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea">Описание</label>
            <textarea class="form-control" id="exampleFormControlTextarea" rows="10" name="description"></textarea>
        </div>
        <div class="btn-group">
            <input class="btn btn-primary"type="submit" value="Сохранить">
            <input class="btn btn-link"type="reset" value="Сбросить">
        </div>
    </form>
</div>
@endsection
