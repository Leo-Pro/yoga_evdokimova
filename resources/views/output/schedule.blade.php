<table class="table table-hover" id="ScheduleList">
    <thead>
        <tr>
            <th scope="col">время</th>
            @foreach ($weekday as $day)
            <th scope="col">{{ \carbon\carbon::parse($day)->format('d.m') }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($listSchedule as $data)
        <tr>
            @foreach ($data as $item)
            @if(gettype($item) == 'string' && $item !== '')
            <th scope="row">{{ $item }}</th>
            @elseif(is_object($item))
            <td>
                {{ $item->title }}<br>
                {{ $item->address }}<br>
                {{ $item->description }}
            </td>
            @elseif($item === '')
            <td></td>
            @endif
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>
