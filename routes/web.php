<?php

use App\Http\Controllers\AdminCertificateController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\AdminProgramController;
use App\Http\Controllers\AdminScheduleController;
use App\Http\Controllers\WeekController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [MainController::class, 'index'])->name('index');
Route::get('/schedule/{schedule}', [WeekController::class, 'index'])->name('schedule.show');
Route::get('/program/{program}', [MainController::class, 'show'])->name('program.show');
Route::post('/feedback', [MainController::class, 'store_feedback'])->name('feedback.store');

Route::prefix('admin')->name('admin.')->group(function (){
    Route::resource('programs', AdminProgramController::class)->names('program');
    Route::resource('certificates', AdminCertificateController::class)->names('certificate');
    Route::resource('schedules', AdminScheduleController::class)->except('show')->names('schedule');
});

